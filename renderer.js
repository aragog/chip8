var Renderer = (function() {
	'use strict';

	var transparency = 'black';

	/**
	 * Constructor.
	 * @param
	 *  canvas: canvas for drawing
	 *  canvasWidth: canvas width
	 *  canvasHeight: canvas height
	 *  cellSize: cells size
	 *  fgColor: color for foreground
	 *  bgColor: color for background
	 */
	function Renderer(canvas, canvasWidth, canvasHeight, cellSize, fgColor, bgColor) {
		this.canvas = canvas;
		this.ctx = canvas.getContext('2d');
		this.canvasWidth = canvasWidth;
		this.canvasHeight = canvasHeight;
		this.cellSize = cellSize;
		this.setCellSize(this.cellSize);

		this.fgColor = fgColor || '#0F0';
		this.bgColor = bgColor || transparency;
	}

	/**
	 * Modify the cells size.
	 * @param
	 *  cellSize: new size of a cell
	 */
	Renderer.prototype.setCellSize = function(cellSize) {
		this.canvas.width = this.canvasWidth * cellSize;
		this.canvas.height = this.canvasHeight * cellSize;
	};

	/**
	 * Render within the canvas from data.
	 * @param
	 *  rendererData: data to render on the canvas
	 */
	Renderer.prototype.render = function(rendererData) {
		var pixels = rendererData.length;
		var startx, starty;

		for (var k = 0; k < pixels; ++k) {
			startx = (k % this.canvasWidth) * this.cellSize;
			starty = Math.floor(k / this.canvasWidth) * this.cellSize;

			this.ctx.fillStyle = rendererData[k] ? this.fgColor : this.bgColor;
			this.ctx.fillRect(startx, starty, this.cellSize, this.cellSize);
		}
	};

	/**
	 * Clear the canvas.
	 */
	Renderer.prototype.clear = function() {
		this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
	};

	return Renderer;
})();