var Keypad = (function() {
	'use strict';

	/*
	 * Keys commonly used with a chip-8 interpreter.
	 */
	var keys = {
		65: 0xA,
		66: 0xB,
		67: 0xC,
		68: 0xD,
		69: 0xE,
		70: 0xF,
		96: 0x0,
		97: 0x1,
		98: 0x2,
		99: 0x3,
		100: 0x4,
		101: 0x5,
		102: 0x6,
		103: 0x7,
		104: 0x8,
		105: 0x9
	};
	
	/**
	 * Constructor.
	 */
	function Keypad() { }

	/**
	 * Translate the key code from the 16-key keypad.
	 * @param
	 *  keycode: key code to translate
	 * @return
	 *  On success, the translated key code, null otherwise
	 */ 
	Keypad.prototype.translate = function(keycode) {
		return keys[keycode];
	};
  
	return Keypad;
})();