var Chip8 = (function() {
	'use strict';

	var maxWidth  = 64;
	var maxHeight = 32;

	var sprites = [
		[0xF0, 0x90, 0x90, 0x90, 0xF0], // 0
		[0x20, 0x60, 0x20, 0x20, 0x70], // 1
		[0xF0, 0x10, 0xF0, 0x80, 0xF0], // 2
		[0xF0, 0x10, 0xF0, 0x10, 0xF0], // 3
		[0x90, 0x90, 0xF0, 0x10, 0x10], // 4
		[0xF0, 0x80, 0xF0, 0x10, 0xF0], // 5
		[0xF0, 0x80, 0xF0, 0x90, 0xF0], // 6
		[0xF0, 0x10, 0x20, 0x40, 0x40], // 7
		[0xF0, 0x90, 0xF0, 0x90, 0xF0], // 8
		[0xF0, 0x90, 0xF0, 0x10, 0xF0], // 9
		[0xF0, 0x90, 0xF0, 0x90, 0x90], // A
		[0xE0, 0x90, 0xE0, 0x90, 0xE0], // B
		[0xF0, 0x80, 0x80, 0x80, 0xF0], // C
		[0xE0, 0x90, 0x90, 0x90, 0xE0], // D
		[0xF0, 0x80, 0xF0, 0x80, 0xF0], // E
		[0xF0, 0x80, 0xF0, 0x80, 0x80]  // F
	];
	
	/**
	 * Constructor
	 */
	function Chip8() {
		this.display = new Uint8Array(maxWidth * maxHeight); // display
		this.keys = new Uint8Array(16); // 16-key hexadecimal keypad
		this.memory = new Uint8Array(4096); // memory
		this.stack = new Uint16Array(16); // stack
		this.v = new Uint8Array(16); // data registers
		this.i = 0; // address register
		this.pc = 0x200; // program counter
		this.sp = -1; // stack pointer (top of stack)
		this.delayTimer = 0; // delay timer
		this.soundTimer = 0; // sound timer
		this.drawFlag = false; // set when display altered
		this.renderer = null; // renderer

		loadSprites(this.memory); // sprites representing the hexadecimal digits
	}

	/**
	 * @return maximal width
     */
	Chip8.prototype.getMaxWidth = function() {
		return maxWidth;
	};

	/**
	 * @return maximal height
	 */
	Chip8.prototype.getMaxHeight = function() {
		return maxHeight;
	};

	/**
	 * Stop the processing.
	 * 
	 * All internal data are reset.
	 */
	Chip8.prototype.stop = function() {
		clearScreen(this.display);

		for (var k = sprites.length, length = this.memory.length; k < length; ++k) {
			this.memory[k] = 0;
		}

		for (var k = 0, length = this.stack.length; k < length; ++k) {
			this.stack[k] = this.v[k] = 0;
		}

		this.i = 0;
		this.pc = 0x200;
		this.sp = -1;
		this.delayTimer = this.soundTimer = 0;
		this.drawFlag = false;

		if (this.renderer) {
			this.renderer.clear();
		}
	};

	/**
	 * Load the program in the memory.
	 * @param
	 *  program: program to load
	 */
	Chip8.prototype.load = function(program) {
		for (var k = 0, length = program.length; k < length; ++k) {
			this.memory[k + 0x200] = program[k];
		}
	};

	/**
	 * A key is pressed.
	 * @param
	 *  key: the key pressed
	 */
	Chip8.prototype.setKey = function(key) {
		this.keys[key] = 1;
	};

	/**
	 * A key is released.
	 * @param
	 *  key: the key released
	 */
	Chip8.prototype.unsetKey = function(key) {
		this.keys[key] = 0;
	};

	/**
	 * Set a new renderer for the display.
	 * @param
	 *  renderer: new renderer
	 */
	Chip8.prototype.setRenderer = function(renderer) {
		this.renderer = renderer;
		renderer.render(this.display);
	};

	/**
	 * Draw the internal data of the interpreter's memory.
	 */
	Chip8.prototype.render = function() {
		if (!this.renderer) {
			throw new Error("You must specify a renderer.");
		}

		this.renderer.render(this.display);
		this.drawFlag = false;
	};

	/**
	 * Start the processing of the interpreter.
     *
     * - emulate cycles
     * - render data
     * - handleTimers
	 */
	Chip8.prototype.start = function() {
		this.running = true;
		var that = this;

		function step() {
			// some cycles executed before display
			for (var k = 0; k < 10; ++k) {
				if (that.running) {
					that.emulateCycle();
				}
			}

			if (that.drawFlag) {
				that.render();
			}

			that.handleTimers();

			if (window.requestAnimationFrame) {
				requestAnimationFrame(step);
			} else {
				setTimeout(step, 33);
			}
		}

		step();
	};
 
    /**
     * Handle the delay and sound timers.
	 */
	Chip8.prototype.handleTimers = function() {
		if (this.delayTimer) {
			--this.delayTimer;
		}

		if (this.soundTimer) {
			--this.soundTimer;
			console.log('Beep !'); // TODO: real sound
		}
	};

	/**
	 * Emulate a cycle of a chip-8 interpreter.
	 */
	Chip8.prototype.emulateCycle = function() {
		var opcode = this.memory[this.pc] << 8 | this.memory[this.pc + 1];
		var nibble = opcode & 0xF000;

		this.pc += 2; // two bytes read

		switch (nibble) {
		case 0x0000:
			switch (opcode & 0x0FFF) {
			case 0x00E0: // clear the display (CLS)
				clearScreen(this.display);
				this.drawFlag = true;
				break;
			case 0x00EE: // return from a subroutine(RET)
				this.pc = this.stack[this.sp--];
				break;
			default:
				throw new Error('no matching case: 0x0000');
			}
			break;
		case 0x1000: // jump to location (JP addr)
			this.pc = opcode & 0x0FFF;
			break;
		case 0x2000: // call subroutine (CALL addr)
			this.stack[++this.sp] = this.pc;
			this.pc = opcode & 0x0FFF;
			break;
		case 0x3000: // conditional skipping next instruction (SE Vx, byte)
			if (this.v[(opcode & 0x0F00) >> 8] === (opcode & 0x00FF)) {
				this.pc += 2;
			}
			break;
		case 0x4000: // conditional skipping next instruction (NSE Vx, byte)
			if (this.v[(opcode & 0x0F00) >> 8] !== (opcode & 0x00FF)) {
				this.pc += 2;
			}
			break;
		case 0x5000: // skipping if register's values are equal (SE Vx, Vy)
			var vx = this.v[(opcode & 0x0F00) >> 8];
			var vy = this.v[(opcode & 0x00F0) >> 4];
			if (vx === vy) {
				this.pc += 2;
			}
			break;
		case 0x6000:
			this.v[(opcode & 0x0F00) >> 8] = (opcode & 0x00FF);
			break;
		case 0x7000: //7xkk
			this.v[(opcode & 0x0F00) >> 8] += (opcode & 0x00FF);
			break;
		case 0x8000:
			var x = (opcode & 0x0F00) >> 8;
			var y = (opcode & 0x00F0) >> 4;

			switch (opcode & 0x000F) {
				case 0x0000:
					this.v[x] = this.v[y];
					break;
				case 0x0001:
					this.v[x] |= this.v[y];
					break;
				case 0x0002:
					this.v[x] &= this.v[y];
					break;
				case 0x0003:
					this.v[x] ^= this.v[y];
					break;
				case 0x0004:
					var sum = this.v[x] + this.v[y];
					this.v[0xF] = (sum > 0xFF) ? 1 : 0; // carry
					this.v[x] = sum;
					break;
				case 0x0005:
					var dif = this.v[x] - this.v[y];
					this.v[0xF] = (dif > 0) ? 1 : 0; // borrow
					this.v[x] = dif;
					break;
				case 0x0006:
					this.v[0xF] = this.v[x] & 0x1;
					this.v[x] >>= 1;
					break;
				case 0x0007:
					var difn = this.v[y] - this.v[x];
					this.v[0xF] = (difn > 0) ? 1 : 0; // borrow
					this.v[x] = difn;
					break;
				case 0x000E:
					this.v[0xF] = this.v[x] & 0x80;
					this.v[x] <<= 1;
					break;
				default:
					throw new Error('no matching case: 0x8000');
			}
			break;
		case 0x9000: // 9xy0
			if (this.v[(opcode & 0x0F00) >> 8] !== this.v[(opcode & 0x00F0) >> 4]) {
				this.pc += 2;
			}
			break;
		case 0xA000: // set value of I (LD I, addr)
			this.i = opcode & 0x0FFF;
			break;
		case 0xB000: // Bnnn
			this.i = (opcode & 0x0FFF) + this.v[0];
			break;
		case 0xC000:
			this.v[(opcode & 0x0F00) >> 8] = getRandomNumber(0xFF) & (opcode & 0x00FF);
			break;
		case 0xD000: // display n-byte sprite starting at memory location I at (Vx, Vy)
			var coordX = this.v[(opcode & 0x0F00) >> 8];
			var coordY = this.v[(opcode & 0x00F0) >> 4];
			var rows = opcode & 0x000F;
			var sprite;

			this.v[0xF] = 0;

			for (var row = 0; row < rows; ++row) {
				sprite = this.memory[row + this.i];
				for (var width = 0, msk = 0x80; width < 8; ++width, msk >>= 1) {
					if (sprite & msk) {
						if (!drawPixel(width + coordX, row + coordY, this.display)) { // collision detection
							this.v[0xF] = 1;
						}
					}
				}
			}

			this.drawFlag = true;
			break;
		case 0xE000:
			switch (opcode & 0x00FF) {
			case 0x9E:
				if (this.keys[this.v[(opcode & 0x0F00) >> 8]]) {
					this.pc += 2;
				}
				break;
			case 0xA1:
				if (!this.keys[this.v[(opcode & 0x0F00) >> 8]]) {
					this.pc += 2;
				}
				break;
			default:
				throw new Error('no matching case: 0xE000');
			}
			break;
		case 0xF000:
			switch (opcode & 0x00FF) {
				case 0x0007:
					this.v[(opcode & 0x0F00) >> 8] = this.delayTimer;
					break;
				case 0x000A:
					var keyPressed = getKey(this.keys);
					this.running = (keyPressed !== null);

					if (this.running) {
						this.v[(opcode & 0x0F00) >> 8] = keyPressed;
					} else {
						this.pc -= 2; // loop until a key is pressed
					}
					break;
				case 0x0015:
					this.delayTimer = this.v[(opcode & 0x0F00) >> 8];
					break;
				case 0x0018:
					this.soundTimer = this.v[(opcode & 0x0F00) >> 8];
					break;
				case 0x001E:
					this.i += this.v[(opcode & 0x0F00) >> 8];
					break;
				case 0x0029:
					this.i = this.v[(opcode & 0x0F00) >> 8] * sprites[0].length;
					break;
				case 0x0033:
					var d = this.v[(opcode & 0x0F00) >> 8];

					for (var k = 2; k >= 0; --k, d = Math.floor(d / 10)) {
						this.memory[this.i + k] = d % 10;
					}
					break;
				case 0x0055:
					for (var k = 0, max = (opcode & 0x0F00) >> 8; k <= max; ++k) {
						this.memory[k + this.i] = this.v[k];
					}
					break;
				case 0x0065:
					for (var k = 0, max = (opcode & 0x0F00) >> 8; k <= max; ++k) {
						this.v[k] = this.memory[k + this.i];
					}		
					break;
				default:
					throw new Error('no matching case: 0xE000');
			}
			break;
		default:
			throw new Error('no matching case: emulateCycle');
		}
	};

	/*
	 * Load into the memory the built-in sprites.
	 * @param
	 *  memory: interpreter's memory
	 */
	function loadSprites(memory) {
		var p = 0;

		for (var k = 0, spriteLength = sprites.length; k < spriteLength; ++k) {
			for (var m = 0, width = sprites[k].length; m < width; ++m, ++p) {
				memory[p] = sprites[k][m];
			}
		}
	}

	/*
	 * Get the key pressed by the user.
	 * @param
	 *  keys: the 16-key hexadecimal keypad
	 * @return
	 *  the key pressed
	 */
	function getKey(keys) {
		for (var k = 0; k < keys.length; ++k) {
			if (keys[k]) {
				return k;
			}
		}

		return null;
	}

	/*
	 * Clear the screen.
	 * @param
	 *  display: display to clear
	 */
	function clearScreen(display) {
		for (var k = 0, length = display.length; k < length; ++k) {
			display[k] = 0;
		}
	}

	/*
	 * @return number in [0, max]
	 */
	function getRandomNumber(max) {
		return Math.floor(Math.random() * (1 + max));
	}

	/*
	 * Draw a pixel located to (x, y) [from the top left corner]
	 * @param
	 *	x: x coordinate
	 *  y: y coordinate
	 * @return
	 *  1 if collision, 0 otherwise
	 */
	function drawPixel(x, y, display) {
		x %= maxWidth;
		y %= maxHeight;

		return (display[x + (y * maxWidth)] ^= 1);
	}
  
	return Chip8;
})();